//
//  GoContactsCell.swift
//  GoContact
//
//  Created by Rahul Singh on 28/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import UIKit

class ContactsCell: UITableViewCell {

    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var favButton: UIButton!
    
    var favButtonAction: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func favouriteButtonAction(_ sender: UIButton) {
        self.favButtonAction?()
    }
    
    func updateCell(_ contact: Contact) {
        self.name.text = "\(contact.firstName ?? "") \(contact.lastName ?? "")"
        self.favButton.setImage(UIImage(named: contact.isFavourite! ? "favourite_button_selected" : "favourite_button"), for: .normal)
        self.profileImage.af_setImage(withURL: URL(string: ApiClient.apiDomain + contact.profiePicUrl!)!)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
