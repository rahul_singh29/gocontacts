//
//  GetContacts.swift
//  GoContact
//
//  Created by Rahul Singh on 27/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation

//{
//    "id": 13987,
//    "first_name": "11836th",
//    "last_name": "show ",
//    "profile_pic": "/images/missing.png",
//    "favorite": true,
//    "url": "http://gojek-contacts-app.herokuapp.com/contacts/13987.json"
//}

struct Contact: Codable {
    var id: Int?
    var firstName: String?
    var lastName: String?
    var profiePicUrl:String?
    var isFavourite: Bool?
    var url: String?
    var email: String?
    var phoneNum: String?
    
    private enum CodingKeys: String, CodingKey {
        case id
        case firstName = "first_name"
        case lastName = "last_name"
        case profiePicUrl = "profile_pic"
        case isFavourite = "favorite"
        case url
        case email
        case phoneNum = "phone_number"
    }
    
    var titleFirstLetter: String {
        return String(self.firstName![self.firstName!.startIndex]).uppercased()
    }
}


extension Contact {
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id)
        firstName = try container.decodeIfPresent(String.self, forKey: .firstName)
        lastName = try container.decodeIfPresent(String.self, forKey: .lastName)
        profiePicUrl = try container.decodeIfPresent(String.self, forKey: .profiePicUrl)
        isFavourite = try container.decodeIfPresent(Bool.self, forKey: .isFavourite)
        url = try container.decodeIfPresent(String.self, forKey: .url)
        email = try container.decodeIfPresent(String.self, forKey: .email)
        phoneNum = try container.decodeIfPresent(String.self, forKey: .phoneNum)
    }
}
