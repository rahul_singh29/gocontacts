//
//  GradientView.swift
//  GoContact
//
//  Created by Rahul Singh on 29/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation
import UIKit

class GradientView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    private func setupView() {
        autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        guard let theLayer = self.layer as? CAGradientLayer else {
            return;
        }
        let colorTop = UIColor(red:1.00, green:1.00, blue:1.00, alpha:0.5).cgColor
        let colorBottom = UIColor(red:0.31, green:0.89, blue:0.76, alpha:0.5).cgColor
        theLayer.colors = [colorTop, colorBottom]
        theLayer.locations = [0.0, 1.0]
        theLayer.frame = self.bounds
        self.layer.cornerRadius = 2.0
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
}
