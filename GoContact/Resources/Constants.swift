//
//  Constants.swift
//  GoContact
//
//  Created by Rahul Singh on 28/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    func removeNavBarBorder() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for:.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
    
    func restoreNavBarBorder() {
        self.navigationController?.navigationBar.setBackgroundImage(nil, for:.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.navigationBar.layoutIfNeeded()
    }
}


extension UIImage {
    func base64() -> String? {
        let imageData: Data = self.pngData()!
        return imageData.base64EncodedString()
    }
}


extension String {
    func imageFromBase64() -> UIImage? {
        guard let data = Data(base64Encoded: self) else { return nil }
        return UIImage(data: data)
    }
}


class Alert {
    static let shared = Alert()
    
    func show(view: UIViewController, withTitle: String = "", message:String) {
        let alert = UIAlertController(title: withTitle, message: message, preferredStyle: .alert)
        let ok = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(ok)
        DispatchQueue.main.async {
            view.present(alert, animated: true, completion: nil)
        }
    }
}


extension UIView {

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }

    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
