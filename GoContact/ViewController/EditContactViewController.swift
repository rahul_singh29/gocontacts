//
//  EditContactViewController.swift
//  GoContact
//
//  Created by Rahul Singh on 27/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import UIKit

class EditContactViewController: UIViewController {

    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    var imagePicker = UIImagePickerController()
    var isContactEditing: Bool = false
    var contact: Contact!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profilePic?.layer.borderColor = UIColor.white.cgColor
        profilePic?.layer.borderWidth = 3
        self.imagePicker.delegate = self
        if self.isContactEditing {
            self.setContactsInTextFieldForEditing()
        }
    }
    
    override func viewWillLayoutSubviews() {
        self.removeNavBarBorder()
    }
    
    func setContactsInTextFieldForEditing() {
        self.firstNameTextField.text = self.contact.firstName ?? ""
        self.lastNameTextField.text = self.contact.lastName ?? ""
        self.mobileTextField.text = self.contact.phoneNum ?? ""
        self.emailTextField.text = self.contact.email ?? ""
        if let picUrlString = self.contact.profiePicUrl {
            self.profilePic.af_setImage(withURL: URL(string: ApiClient.apiDomain + picUrlString)!)
        }
    }

// MARK: -------- BUTTON ACTION ---------
    @IBAction func doneButtonAction(_ sender: UIBarButtonItem) {
        if self.firstNameTextField.text!.isEmpty || self.lastNameTextField.text!.isEmpty || self.mobileTextField.text!.isEmpty || self.emailTextField.text!.isEmpty {
            Alert.shared.show(view: self, message: "Mandatory fields are: First Name, Last Name, Mobile, Email")
            return
        }
        LoadingOverlay.shared.showOverlay(view: self.view)
        if self.isContactEditing {
            self.updateContact()
        } else {
            self.addContact()
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
// MARK: --------- API CALLS ---------
    func updateContact() {
        let params = UpdateDetails(firstName: self.firstNameTextField.text, lastName: self.lastNameTextField.text, mobileNum: self.mobileTextField.text, email: self.emailTextField.text, profilePic: self.contact.profiePicUrl)
        ApiClient.shared.updateContacts(of: self.contact.id!, params: params, handler: RegularRequestCompletion<Contact>(success: { (response) in
            LoadingOverlay.shared.hide()
            Alert.shared.show(view: self, message: "Contact Updated Successfully")
        }, failure: { (error) in
            LoadingOverlay.shared.hide()
            Alert.shared.show(view: self, message: "Something went wrong")
        }))
    }
    
    func addContact() {
        let params = UpdateDetails(firstName: self.firstNameTextField.text, lastName: self.lastNameTextField.text, mobileNum: self.mobileTextField.text, email: self.emailTextField.text, profilePic: "")
        ApiClient.shared.addContact(params: params, handler: RegularRequestCompletion<Contact>(success: { (response) in
            LoadingOverlay.shared.hide()
            self.firstNameTextField.text = ""
            self.lastNameTextField.text = ""
            self.mobileTextField.text = ""
            self.emailTextField.text = ""
            Alert.shared.show(view: self, message: "Contact Added Successfully")
        }, failure: { (error) in
            LoadingOverlay.shared.hide()
            Alert.shared.show(view: self, message: "Something went wrong")
        }))
    }
}

// MARK: -------- IMAGE PICKER ---------
extension EditContactViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBAction func cameraButtonAction(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.profilePic.contentMode = .scaleAspectFit
            self.profilePic.image = pickedImage
            _ = pickedImage.base64()
            //upload string image to server
        }
    }
}
