//
//  ContactDetailsViewController.swift
//  GoContact
//
//  Created by Rahul Singh on 27/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import UIKit
import AlamofireImage
import MessageUI

class ContactDetailsViewController: UIViewController {

    @IBOutlet weak var contactProfilePic: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mobileNumLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var favButton: UIButton!
    
    var contact: Contact!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contactProfilePic!.layer.borderColor = UIColor.white.cgColor
        contactProfilePic!.layer.borderWidth = 3
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getContactDetails()
    }
    
    override func viewWillLayoutSubviews() {
        self.removeNavBarBorder()
    }
    
// MARK: ------- GET CONTACT DETAILS -------
    func getContactDetails() {
        LoadingOverlay.shared.showOverlay(view: self.view)
        ApiClient.shared.getContactDetails(of: self.contact.id!, handler: RegularRequestCompletion<Contact>(success: { (response) in
            LoadingOverlay.shared.hide()
            self.contactProfilePic.af_setImage(withURL: URL(string: ApiClient.apiDomain + response.profiePicUrl!)!)
            self.nameLabel.text = response.firstName! + " " + response.lastName!
            self.mobileNumLabel.text = response.phoneNum ?? ""
            self.emailLabel.text = response.email ?? ""
            self.favButton.setImage(UIImage(named: response.isFavourite! ? "favourite_button_selected" : "favourite_button"), for: .normal)
            self.contact.firstName = response.firstName ?? ""
            self.contact.lastName = response.lastName ?? ""
            self.contact.phoneNum = response.phoneNum ?? ""
            self.contact.email = response.email ?? ""
        }, failure: { (error) in
            LoadingOverlay.shared.hide()
            Alert.shared.show(view: self, message: "Something went wrong")
        }))
    }

 
// MARK: ------- BUTTON ACTIONS -------
    @IBAction func messageButtonAction(_ sender: UIButton) {
        guard let phoneNum = self.mobileNumLabel.text else {
            Alert.shared.show(view: self, message: "Please Update your mobile number, then try again")
            return
        }
        if MFMessageComposeViewController.canSendText() {
            let controller = MFMessageComposeViewController()
            controller.body = "Hi, This is just a testing."
            controller.recipients = [phoneNum]
            controller.messageComposeDelegate = self
            self.present(controller, animated: true, completion: nil)
        } else {
            Alert.shared.show(view: self, message: "Something went wrong")
        }
    }
    
    @IBAction func callButtonAction(_ sender: UIButton) {
        guard let phoneNum = self.mobileNumLabel.text else {
            Alert.shared.show(view: self, message: "Please Update your mobile number, then try again")
            return
        }
        if let url = URL(string: "tel://\(phoneNum)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    @IBAction func emailButtonAction(_ sender: UIButton) {
        guard let email = self.emailLabel.text else {
            Alert.shared.show(view: self, message: "Please Update your email address, then try again")
            return
        }
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([email])
            mail.setSubject("This is Email Subject.")
            mail.setMessageBody("Hi, I am message body. Thanks", isHTML: false)
            present(mail, animated: true)
        } else {
            Alert.shared.show(view: self, message: "Please setup your mail")
        }
    }
    
    @IBAction func favouriteButtonAction(_ sender: UIButton) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        let params = UpdateFav(isFav: !self.contact.isFavourite!)
        ApiClient.shared.updateContacts(of: contact.id!, params: params, handler: RegularRequestCompletion<Contact>(success: { (response) in
            print(response)
            LoadingOverlay.shared.hide()
            self.favButton.setImage(UIImage(named: response.isFavourite! ? "favourite_button_selected" : "favourite_button"), for: .normal)
            self.contact.isFavourite = response.isFavourite!
        }, failure: { (error) in
            LoadingOverlay.shared.hide()
            Alert.shared.show(view: self, message: "Something went wrong")
        }))
    }
    
  

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? EditContactViewController {
            destination.isContactEditing = true
            destination.contact = self.contact
        }
    }

}

extension ContactDetailsViewController: MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
}
