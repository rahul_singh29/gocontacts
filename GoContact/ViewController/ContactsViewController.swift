//
//  ContactsViewController.swift
//  GoContact
//
//  Created by Rahul Singh on 27/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import UIKit
import AlamofireImage


class ContactsViewController: UITableViewController {
    
    var contactList: [Contact]?
    var sortedFirstLetters: [String] = []
    var contactSections: [[Contact]] = [[Contact]]() {
        didSet {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        LoadingOverlay.shared.showOverlay(view: self.view)
        self.getContacts()
    }
    
    override func viewWillLayoutSubviews() {
        self.restoreNavBarBorder()
    }
    

// MARK: ------- GET CONTACT LIST -------
    func getContacts() {
        ApiClient.shared.getContacts(handler: RegularRequestCompletion<[Contact]>(success: { (cList) in
            if cList.count > 0 {
                self.contactList = cList
                self.sortedFirstLetters = Dictionary(grouping: self.contactList!, by: {String($0.firstName!.uppercased().prefix(1))}).keys.sorted().map({ $0 })
                self.contactSections = self.sortedFirstLetters.map { firstLetter in
                    return self.contactList!.filter { $0.titleFirstLetter == firstLetter }.sorted { $0.firstName! < $1.firstName! }
                }
                print(self.contactSections)
                LoadingOverlay.shared.hide()
            }
        }, failure: { (error) in
            LoadingOverlay.shared.hide()
            print(error)
        }))
    }
    
// MARK: ------- TABLE VIEW DELEGATE FUNCTION -------
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if sortedFirstLetters.count == 0 {
            return ""
        }
        return sortedFirstLetters[section]
    }
    
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor(red:0.91, green:0.91, blue:0.91, alpha:1.0)
    }

    override func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return sortedFirstLetters
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return contactSections.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactSections[section].count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let contactCell = tableView.dequeueReusableCell(withIdentifier: "ContactsCell") as! ContactsCell
        let kContact = contactSections[indexPath.section][indexPath.row]
        contactCell.updateCell(kContact)
        contactCell.favButtonAction = { [unowned self] in
            self.updateContactList(at: indexPath, cell: contactCell)
        }
        return contactCell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(contactSections[indexPath.section][indexPath.row])
        self.performSegue(withIdentifier: "segue_to_details", sender: self.contactSections[indexPath.section][indexPath.row])
    }
    
    func updateContactList(at indexPath : IndexPath, cell: ContactsCell) {
        var kContact = contactSections[indexPath.section][indexPath.row]
        let params = UpdateFav(isFav: !kContact.isFavourite!)
        ApiClient.shared.updateContacts(of: kContact.id!, params: params, handler: RegularRequestCompletion<Contact>(success: { (response) in
            print(response)
            cell.favButton.setImage(UIImage(named: response.isFavourite! ? "favourite_button_selected" : "favourite_button"), for: .normal)
            kContact.isFavourite = response.isFavourite!
            self.contactSections[indexPath.section][indexPath.row] = kContact
        }, failure: { (error) in
        }))
    }
   
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? ContactDetailsViewController {
            destination.contact = (sender as! Contact)
        }
    }
}
