//
//  UpdateContact.swift
//  GoContact
//
//  Created by Rahul Singh on 28/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation

extension ApiClient {
    
    func updateContacts(of id: Int, params: RequestParameters, handler: RegularRequestCompletion<Contact>) {
        request(method: .put, path: "/contacts/\(id).json", parameters: params,handler: RequestCompletion.regular(completion: handler))
    }
    
    func addContact(params: RequestParameters, handler: RegularRequestCompletion<Contact>) {
        request(method: .post, path: "/contacts.json", parameters: params,handler: RequestCompletion.regular(completion: handler))
    }
    
}

struct UpdateDetails: RequestParameters {
    let firstName: String?
    let lastName: String?
    let mobileNum: String?
    var email: String?
    var profilePic: String?
    
    func parameters() -> [String : Any] {
        return [
            "first_name": firstName ?? "",
            "last_name": lastName ?? "",
            "email": email ?? "",
            "phone_number": "+919980123412",
            "profile_pic": "/images/missing.png"
        ]
    }
}
struct UpdateFav: RequestParameters {
    let isFav: Bool
    
    func parameters() -> [String : Any] {
        return [
            "favorite": isFav
        ]
    }
}
