//
//  GoContactsRequest.swift
//  GoContact
//
//  Created by Rahul Singh on 27/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation
import Alamofire

extension ApiClient {
    
    func getContacts(handler: RegularRequestCompletion<[Contact]>) {
        request(path: "/contacts.json", handler: RequestCompletion.regular(completion: handler))
    }

    func getContactDetails(of id: Int, handler: RegularRequestCompletion<Contact>) {
        request(path: "/contacts/\(id).json", handler: RequestCompletion.regular(completion: handler))
    }
}
