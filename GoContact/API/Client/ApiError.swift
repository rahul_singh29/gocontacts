//
//  ApiError.swift
//  GoContact
//
//  Created by Rahul Singh on 27/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation

extension NSError {
    static func apiDomainError(_ json: [String: AnyObject]) -> NSError? {
        return nil
    }
    
    static func errorWithMessage(_ message: String) -> NSError {
        return NSError(domain: "AppDomain", code: 0, userInfo: [NSLocalizedDescriptionKey : message])
    }
}
