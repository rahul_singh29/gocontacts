//
//  RequestCompletionHandler.swift
//  GoContact
//
//  Created by Rahul Singh on 27/11/19.
//  Copyright © 2019 Rahul Singh. All rights reserved.
//

import Foundation


struct RegularRequestCompletion<Response> {
    let success: (Response) -> ()
    let failure: ((Error) -> ())?
    
    init(success: @escaping (Response) -> (), failure: ((Error) -> ())? = nil) {
        self.success = success
        self.failure = failure
    }
}

enum RequestCompletion<Response> {
    case regular(completion: RegularRequestCompletion<Response>)
    
    var failure: ((Error) -> ())? {
        switch self {
            case .regular(let completion): return completion.failure
        }
    }
}
